class BaseSwarmer{

	float radius;
	float calculatedScale = 1;
	float scaledRadius = 1;
	color displayColor;

	float xPos;
	float yPos;
	float zPos;
	float speed;
	float rotation = 0;
	float rotationSpeed;
	float goalAngle = 0;
	float goalX = 0;
	float goalY = 0;
	float goalZ = 0;

	void BaseSwarmer(){

	}

	void display(){
		
		scaledRadius = radius * calculatedScale;
		if(scaledRadius > -1){
			fill(displayColor);
			ellipse(this.xPos * this.calculatedScale, this.yPos * this.calculatedScale, this.scaledRadius, this.scaledRadius);
		}
	}
}