int width = 800;
int height = 800;
int halfWidth;
int halfHeight;

int focalLength = 1000;

int leaderRadius = 10;
int leaderTime = 0;
float leaderSpeed = 10;
float leaderRotationSpeed = 20;
int leaderRunTime = 10;

int swarmRadius = 10;
float swarmTwitch = 15;
float swarmRotationSpeed = 10;
float swarmSpeed = 20;

int swarmAmount = 500;

Leader leader;
Follower[] followers;

void setup(){
  size(width, height);
  frameRate(30);

  halfWidth = this.width / 2;
  halfHeight = this.height / 2;

  leader = new Leader();
  leader.radius = this.leaderRadius;
  leader.xPos = width / 2;
  leader.yPos = height / 2;
  leader.zPos = 0;
  leader.rotationSpeed = this.leaderRotationSpeed;
  leader.speed = this.leaderSpeed;
  leader.runTime = this.leaderRunTime;
  leader.displayColor = #FF0000;


  followers = new Follower[swarmAmount];

  int i = 0;
  while(i < swarmAmount){
  	Follower follower = new Follower();
  	follower.radius = this.swarmRadius;
  	follower.xPos = width / 2;
  	follower.yPos = height / 2;
  	follower.zPos = 0;
  	follower.rotationSpeed = this.swarmRotationSpeed;
  	follower.speed = this.swarmSpeed;
  	follower.twitch = this.swarmTwitch;
  	follower.displayColor = #FF0000;
  	followers[i] = follower;
  	i++;
  }
}

void draw(){
  background(0, 0, 0);

  moveLeader();
  leader.display();

  int i = 0;
  while(i < this.swarmAmount){
  	Follower item = followers[i];
  	moveSwarmItem(item);
  	item.display();
  	i++;
  }
}

void moveLeader(){
	if(this.leaderTime == 0){
		leader.goalX = random(1) * width;
		leader.goalY = random(1) * height;
		leader.goalZ = random(1) * (width / 2);
		leaderTime = leader.runTime;
	}

	setGoalAngle(leader);
	setRotation(leader);
	setPosition(leader);

	this.leaderTime--;
}

void moveSwarmItem(Follower follower){
	follower.goalX = leader.xPos;
	follower.goalY = leader.yPos;
	follower.goalZ = leader.zPos;

	setGoalAngle(follower);
	setRotation(follower);
	follower.rotation += (random(1) * follower.twitch) - (follower.twitch * 0.5);
	setPosition(follower);
}

void setGoalAngle(BaseSwarmer item){
	item.goalAngle = atan2(item.goalY - item.yPos, item.goalX - item.xPos);
	item.goalAngle = item.goalAngle * 180 / PI;
	item.goalAngle = angleFix(item.goalAngle - item.rotation);
}

void setRotation(BaseSwarmer item){
	if(item.goalAngle < 180){
		item.rotation += item.rotationSpeed;
	} else {
		item.rotation -= item.rotationSpeed;
	}
}

void setPosition(BaseSwarmer item){
	item.xPos += calcCos(item.rotation) * item.rotationSpeed;
	item.yPos += calcSin(item.rotation) * item.rotationSpeed;
	item.zPos += calcCos(item.rotation) * item.rotationSpeed;

	item.calculatedScale = this.focalLength/(item.zPos + this.focalLength);
}

float calcCos(float angle){
	return cos(angle * PI/180);
}

float calcSin(float angle){
	return sin(angle * PI/180);
}

float angleFix(float angle){
	if(angle > 360){
		angle = angle % 360;
	}
	while(angle < 0){
		angle += 360;
	}
	return angle;
}

